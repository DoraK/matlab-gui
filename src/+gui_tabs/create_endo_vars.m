function new_handles = create_endo_vars(handles, command)
% function new_handles = create_endo_vars(handles, command)
% creates uitabgroup with endogenous variables
%
% INPUTS
%   handles:            handles of all GUI elements on current tab inside
%                       which uitabgroup is created
%   command:            string representing command that we're creating
%                       endo vars for
%
% OUTPUTS
%   new_handles: updated handles of all GUI elements on current tab
%
% SPECIAL REQUIREMENTS
%   none

% Copyright (C) 2019-2020 Dynare Team
%
% This file is part of Dynare.
%
% Dynare is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% Dynare is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with Dynare.  If not, see <http://www.gnu.org/licenses/>.

global model_settings oo_ ys0_

handles.varsTabGroup = uitabgroup(handles.uipanelVars, 'Position', [0 0 1 1]);

varidxs = find([model_settings.variables{:, 4}]);
column_names = {'Active', 'Name'};
switch command
    case 'perfect_foresight'
        column_names = {'Active', 'Name', 'Initial Condition', 'Terminal Condition'};
    case 'stoch_simul'
        column_names = {'Active', 'Name', 'Initial Condition'};
    case 'calib_smoother'
        column_names = {'Active', 'Name', 'Long Name'};
    case 'forecast'
        column_names = {'Active', 'Name', 'Initial Condition', 'New Value'};
        varidxs = 1:size(model_settings.variables,1);
end
nvars = length(varidxs);
editable = true(1, size(column_names, 2));
editable(2) = false;
data = cell(nvars, size(column_names, 2));

data(:, 1) = {false};
data(:, 2) = model_settings.variables(varidxs, 1);
if strcmp(command, 'stoch_simul') || strcmp(command, 'forecast')
    data(:, 3) = num2cell(oo_.steady_state(varidxs));
    if strcmp(command, 'forecast')
        data(:, 4) = num2cell(oo_.steady_state(varidxs));
        editable(3) = false;
        editable(4) = true;
    end
elseif strcmp(command, 'perfect_foresight')
    if ~isempty(ys0_)
        data(:, 3) = num2cell(ys0_(varidxs));
    end
    data(:, 4) = num2cell(oo_.steady_state(varidxs));
elseif strcmp(command, 'calib_smoother')
    data(:, 3) = model_settings.variables(varidxs, 3);
    editable(3) = false;
end

tabTitle = 'All'; % fix this later if we still want to support it
new_tab = uitab(handles.varsTabGroup, 'Title',tabTitle , 'UserData', 1);
currentPanel = uipanel('Parent', new_tab, ...
    'BackgroundColor', 'white', ...
    'BorderType', 'none');

handles.endoTable = uitable(currentPanel, ...
    'Data', data, ...
    'Units', 'normalized', ...
    'ColumnName', column_names, ...
    'ColumnEditable', editable, ...
    'RowName', [], ...
    'Position', [0 0 1 1]);

new_handles = handles;
end
