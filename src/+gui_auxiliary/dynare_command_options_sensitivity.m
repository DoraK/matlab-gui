function dynare_command_options_sensitivity()
% function dynare_command_options_sensitivity()
% creates Dynare_GUI internal structure which holds possible options for
% sensitivity command
%
% Each command option has following four fields: name, default value (if any), type and description.
%
% INPUTS
%   none
%
% OUTPUTS
%   none
%
% SPECIAL REQUIREMENTS
%   none

% Copyright (C) 2019-2020 Dynare Team
%
% This file is part of Dynare.
%
% Dynare is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% Dynare is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with Dynare.  If not, see <http://www.gnu.org/licenses/>.

global dynare_gui_
dynare_gui_.sensitivity = {};

%% Group 1: sampling
num = 1;
dynare_gui_.sensitivity.sampling{num,1} = 'Nsam';    % name
dynare_gui_.sensitivity.sampling{num,2} = '2048';    % default value
dynare_gui_.sensitivity.sampling{num,3} = 'INTEGER'; % type
dynare_gui_.sensitivity.sampling{num,4} = 'Size of the Monte-Carlo sample.'; % additinal comment

num = num + 1;
dynare_gui_.sensitivity.sampling{num,1} = 'ilptau';
dynare_gui_.sensitivity.sampling{num,2} = 1;
dynare_gui_.sensitivity.sampling{num,3} = {'1', '0'};
dynare_gui_.sensitivity.sampling{num,4} = 'If equal to 1 use lptau quasi-Monte-Carlo. Otherwise use LHS Monte-Carlo.';

num = num + 1;
dynare_gui_.sensitivity.sampling{num,1} = 'pprior';
dynare_gui_.sensitivity.sampling{num,2} = 1;
dynare_gui_.sensitivity.sampling{num,3} = {'1', '0'};
dynare_gui_.sensitivity.sampling{num,4} = 'If equal to 1, sample from the prior distributions. If equal to 0, sample from the multivariate normal.';

num = num + 1;
dynare_gui_.sensitivity.sampling{num,1} = 'prior_range';
dynare_gui_.sensitivity.sampling{num,2} = 1;
dynare_gui_.sensitivity.sampling{num,3} = {'1', '0'};
dynare_gui_.sensitivity.sampling{num,4} = 'If equal to 1, sample uniformly from prior ranges. If equal to 0, sample from prior distributions.';

num = num + 1;
dynare_gui_.sensitivity.sampling{num,1} = 'morris';
dynare_gui_.sensitivity.sampling{num,2} = 0;
dynare_gui_.sensitivity.sampling{num,3} = 'INTEGER';
dynare_gui_.sensitivity.sampling{num,4} = 'If equal to 0, ANOVA mapping (Type I error) If equal to 1, Screening analysis (Type II error) If equal to 2, Analytic derivatives (similar to Type II error, only valid when identification=1).';

num = num + 1;
dynare_gui_.sensitivity.sampling{num,1} = 'morris_nliv';
dynare_gui_.sensitivity.sampling{num,2} = '6';
dynare_gui_.sensitivity.sampling{num,3} = 'INTEGER';
dynare_gui_.sensitivity.sampling{num,4} = 'Number of levels in Morris design.';

num = num + 1;
dynare_gui_.sensitivity.sampling{num,1} = 'morris_ntra';
dynare_gui_.sensitivity.sampling{num,2} = '20';
dynare_gui_.sensitivity.sampling{num,3} = 'INTEGER';
dynare_gui_.sensitivity.sampling{num,4} = 'Number trajectories in Morris design.';

num = num + 1;
dynare_gui_.sensitivity.sampling{num,1} = 'ppost';
dynare_gui_.sensitivity.sampling{num,2} = 0;
dynare_gui_.sensitivity.sampling{num,3} = 'check_option';
dynare_gui_.sensitivity.sampling{num,4} = 'If equal to 1, use Metropolis posterior sample. If equal to 0, do not use Metropolis posterior sample. NB: This overrides any other sampling option.';

num = num + 1;
dynare_gui_.sensitivity.sampling{num,1} = 'neighborhood_width';
dynare_gui_.sensitivity.sampling{num,2} = '0';
dynare_gui_.sensitivity.sampling{num,3} = 'DOUBLE';
dynare_gui_.sensitivity.sampling{num,4} = 'When pprior=0 and ppost=0, allows for the sampling of param- eters around the value specified in the mode_file, in the range xparam1± |xparam1 × neighborhood_width|. Default: 0';

%% Group 2: stability mapping
num = 1;
dynare_gui_.sensitivity.stability_mapping{num,1} = 'stab';
dynare_gui_.sensitivity.stability_mapping{num,2} = 1;
dynare_gui_.sensitivity.stability_mapping{num,3} = {'1', '0'};
dynare_gui_.sensitivity.stability_mapping{num,4} = 'If equal to 1, perform stability mapping. If equal to 0, do not perform stability mapping.';

num = num + 1;
dynare_gui_.sensitivity.stability_mapping{num,1} = 'load_stab';
dynare_gui_.sensitivity.stability_mapping{num,2} = 0;
dynare_gui_.sensitivity.stability_mapping{num,3} = 'check_option';
dynare_gui_.sensitivity.stability_mapping{num,4} = 'If equal to 1, load a previously created sample. If equal to 0, generate a new sample';

num = num + 1;
dynare_gui_.sensitivity.stability_mapping{num,1} = 'alpha2_stab';
dynare_gui_.sensitivity.stability_mapping{num,2} = '0';
dynare_gui_.sensitivity.stability_mapping{num,3} = 'DOUBLE';
dynare_gui_.sensitivity.stability_mapping{num,4} = 'Critical value for correlations ? in filtered samples: plot couples of parmaters with |?| > alpha2_stab';

num = num + 1;
dynare_gui_.sensitivity.stability_mapping{num,1} = 'pvalue_ks';
dynare_gui_.sensitivity.stability_mapping{num,2} = '0.001';
dynare_gui_.sensitivity.stability_mapping{num,3} = 'DOUBLE';
dynare_gui_.sensitivity.stability_mapping{num,4} = 'The threshold pvalue for significant Kolmogorov-Smirnov test (i.e. plot parameters with pvalue < pvalue_ks)';

num = num + 1;
dynare_gui_.sensitivity.stability_mapping{num,1} = 'pvalue_corr';
dynare_gui_.sensitivity.stability_mapping{num,2} = '1e-5';
dynare_gui_.sensitivity.stability_mapping{num,3} = 'DOUBLE';
dynare_gui_.sensitivity.stability_mapping{num,4} = 'The threshold pvalue for significant correlation in filtered samples (i.e. plot bivariate samples when pvalue < pvalue_corr)';

%% Group 3: Reduced Form Mapping
num = 1;
dynare_gui_.sensitivity.reduced_form_mapping{num,1} = 'redform';
dynare_gui_.sensitivity.reduced_form_mapping{num,2} = 0;
dynare_gui_.sensitivity.reduced_form_mapping{num,3} = 'check_option';
dynare_gui_.sensitivity.reduced_form_mapping{num,4} = 'If equal to 1, prepare Monte-Carlo sample of reduced form matrices. If equal to 0, do not prepare Monte-Carlo sample of reduced form matrices.';

num = num + 1;
dynare_gui_.sensitivity.reduced_form_mapping{num,1} = 'load_redform';
dynare_gui_.sensitivity.reduced_form_mapping{num,2} = 0;
dynare_gui_.sensitivity.reduced_form_mapping{num,3} = 'check_option';
dynare_gui_.sensitivity.reduced_form_mapping{num,4} = 'If equal to 1, load previously estimated mapping. If equal to 0, estimate the mapping of the reduced form model.';

num = num + 1;
dynare_gui_.sensitivity.reduced_form_mapping{num,1} = 'logtrans_redform';
dynare_gui_.sensitivity.reduced_form_mapping{num,2} = 0;
dynare_gui_.sensitivity.reduced_form_mapping{num,3} = 'check_option';
dynare_gui_.sensitivity.reduced_form_mapping{num,4} = 'If equal to 1, use log-transformed entries. If equal to 0, use raw entries.';

num = num + 1;
dynare_gui_.sensitivity.reduced_form_mapping{num,1} = 'threshold_redform';
dynare_gui_.sensitivity.reduced_form_mapping{num,2} = '';
dynare_gui_.sensitivity.reduced_form_mapping{num,3} = '[DOUBLE DOUBLE]';
dynare_gui_.sensitivity.reduced_form_mapping{num,4} = 'The range over which the filtered Monte-Carlo entries of the reduced form coefficients should be analyzed. The first number is the lower bound and the second is the upper bound. An empty vector indicates that these entries will not be filtered.';

num = num + 1;
dynare_gui_.sensitivity.reduced_form_mapping{num,1} = 'ksstat_redform ';
dynare_gui_.sensitivity.reduced_form_mapping{num,2} = '0.001';
dynare_gui_.sensitivity.reduced_form_mapping{num,3} = 'DOUBLE';
dynare_gui_.sensitivity.reduced_form_mapping{num,4} = 'Critical value for Smirnov statistics d when reduced form entries are filtered.';

num = num + 1;
dynare_gui_.sensitivity.reduced_form_mapping{num,1} = 'alpha2_redform ';
dynare_gui_.sensitivity.reduced_form_mapping{num,2} = '1e-5';
dynare_gui_.sensitivity.reduced_form_mapping{num,3} = 'DOUBLE';
dynare_gui_.sensitivity.reduced_form_mapping{num,4} = 'Critical value for correlations ? when reduced form entries are filtered.';

num = num + 1;
dynare_gui_.sensitivity.reduced_form_mapping{num,1} = 'namendo';
dynare_gui_.sensitivity.reduced_form_mapping{num,2} = '';
dynare_gui_.sensitivity.reduced_form_mapping{num,3} = 'VARIABLE_LIST';
dynare_gui_.sensitivity.reduced_form_mapping{num,4} = 'List of endogenous variables. ?:? indicates all endogenous variables.';

num = num + 1;
dynare_gui_.sensitivity.reduced_form_mapping{num,1} = 'namlagendo';
dynare_gui_.sensitivity.reduced_form_mapping{num,2} = '';
dynare_gui_.sensitivity.reduced_form_mapping{num,3} = 'VARIABLE_LIST';
dynare_gui_.sensitivity.reduced_form_mapping{num,4} = 'List of lagged endogenous variables. ?:? indicates all lagged endogenous variables. Analyze entries [namendo×namlagendo]';

num = num + 1;
dynare_gui_.sensitivity.reduced_form_mapping{num,1} = 'namexo';
dynare_gui_.sensitivity.reduced_form_mapping{num,2} = '';
dynare_gui_.sensitivity.reduced_form_mapping{num,3} = 'VARIABLE_LIST';
dynare_gui_.sensitivity.reduced_form_mapping{num,4} = 'List of exogenous variables. ?:? indicates all exogenous variables. Analyze entries [namendo×namexo].';

%% Group 4: RMSE
num = 1;
dynare_gui_.sensitivity.rmse{num,1} = 'rmse';
dynare_gui_.sensitivity.rmse{num,2} = 0;
dynare_gui_.sensitivity.rmse{num,3} = 'check_option';
dynare_gui_.sensitivity.rmse{num,4} = 'If equal to 1, perform RMSE analysis. If equal to 0, do not perform RMSE analysis.';

num = num + 1;
dynare_gui_.sensitivity.rmse{num,1} = 'load_rmse';
dynare_gui_.sensitivity.rmse{num,2} = 0;
dynare_gui_.sensitivity.rmse{num,3} = 'check_option';
dynare_gui_.sensitivity.rmse{num,4} = 'If equal to 1, load previous RMSE analysis. If equal to 0, make a new RMSE analysis.';

num = num + 1;
dynare_gui_.sensitivity.rmse{num,1} = 'lik_only';
dynare_gui_.sensitivity.rmse{num,2} = 0;
dynare_gui_.sensitivity.rmse{num,3} = 'check_option';
dynare_gui_.sensitivity.rmse{num,4} = 'If equal to 1, compute only likelihood and posterior. If equal to 0, compute RMSE?s for all observed series.';

num = num + 1;
dynare_gui_.sensitivity.rmse{num,1} = 'var_rmse';
dynare_gui_.sensitivity.rmse{num,2} = 'varobs';
dynare_gui_.sensitivity.rmse{num,3} = 'VARIABLE_LIST';
dynare_gui_.sensitivity.rmse{num,4} = 'List of observed series to be considered. ?:? indicates all observed variables.';

num = num + 1;
dynare_gui_.sensitivity.rmse{num,1} = 'pfilt_rmse';
dynare_gui_.sensitivity.rmse{num,2} = '0.1';
dynare_gui_.sensitivity.rmse{num,3} = 'DOUBLE';
dynare_gui_.sensitivity.rmse{num,4} = 'Filtering threshold for RMSE?s.';

num = num + 1;
dynare_gui_.sensitivity.rmse{num,1} = 'istart_rmse';
dynare_gui_.sensitivity.rmse{num,2} = 'presample+1';
dynare_gui_.sensitivity.rmse{num,3} = 'INTEGER';
dynare_gui_.sensitivity.rmse{num,4} = 'Value at which to start computing RMSE?s (use 2 to avoid big intitial error).';

num = num + 1;
dynare_gui_.sensitivity.rmse{num,1} = 'alpha_rmse';
dynare_gui_.sensitivity.rmse{num,2} = '0.001';
dynare_gui_.sensitivity.rmse{num,3} = 'DOUBLE';
dynare_gui_.sensitivity.rmse{num,4} = 'Critical value for Smirnov statistics d: plot parameters with d > alpha_rmse.';

num = num + 1;
dynare_gui_.sensitivity.rmse{num,1} = 'alpha2_rmse';
dynare_gui_.sensitivity.rmse{num,2} = '1e-5';
dynare_gui_.sensitivity.rmse{num,3} = 'DOUBLE';
dynare_gui_.sensitivity.rmse{num,4} = 'Critical value for correlation ?: plot couples of parmaters with |?| = alpha2_rmse.';

num = num + 1;
dynare_gui_.sensitivity.rmse{num,1} = 'datafile';
dynare_gui_.sensitivity.rmse{num,2} = '';
dynare_gui_.sensitivity.rmse{num,3} = 'FILENAME';
dynare_gui_.sensitivity.rmse{num,4} = 'The datafile: a .m file, a .mat file, a .csv file, or a .xls/.xlsx file.';

num = num + 1;
dynare_gui_.sensitivity.rmse{num,1} = 'nobs';
dynare_gui_.sensitivity.rmse{num,2} = '';
dynare_gui_.sensitivity.rmse{num,3} = 'INTEGER or [INTEGER1:INTEGER2]';
dynare_gui_.sensitivity.rmse{num,4} = 'The number of observations following [first obs] to be used. Default: all observations in the file after first_obs. If type is [INTEGER1:INTEGER2] runs a recursive sensitivity and forecast for samples of size ranging of INTEGER1 to INTEGER2. Option forecast must also be specified. The forecasts are stored in the RecursiveForecast field of the results structure.';

num = num + 1;
dynare_gui_.sensitivity.rmse{num,1} = 'first_obs';
dynare_gui_.sensitivity.rmse{num,2} = '1';
dynare_gui_.sensitivity.rmse{num,3} = 'INTEGER or [INTEGER1:INTEGER2]';
dynare_gui_.sensitivity.rmse{num,4} = 'The number of the first observation to be used. In case of estimating a DSGE-VAR, first_obs needs to be larger than the number of lags. Default: 1';

num = num + 1;
dynare_gui_.sensitivity.rmse{num,1} = 'prefilter';
dynare_gui_.sensitivity.rmse{num,2} = '0';
dynare_gui_.sensitivity.rmse{num,3} = 'INTEGER';
dynare_gui_.sensitivity.rmse{num,4} = 'A value of 1 means that the sensitivity procedure will demean each data series by its empirical mean. Default: 0, i.e. no prefiltering.';

num = num + 1;
dynare_gui_.sensitivity.rmse{num,1} = 'presample';
dynare_gui_.sensitivity.rmse{num,2} = '0';
dynare_gui_.sensitivity.rmse{num,3} = 'INTEGER';
dynare_gui_.sensitivity.rmse{num,4} = 'The number of observations after [first obs], page 57 to be skipped before evaluating the likelihood. These presample observations do not enter the likelihood, but are used as a training sample for starting the Kalman filter iterations. This option is incompatible with estimating a DSGE-VAR. Default: 0';

num = num + 1;
dynare_gui_.sensitivity.rmse{num,1} = 'nograph';
dynare_gui_.sensitivity.rmse{num,2} = 0;
dynare_gui_.sensitivity.rmse{num,3} = 'check_option';
dynare_gui_.sensitivity.rmse{num,4} = 'Do not create graphs (which implies that they are not saved to the disk nor displayed). If this option is not used, graphs will be saved to disk (to the format specified by graph_format option, except if graph_format=none) and displayed to screen (unless nodisplay option is used).';

num = num + 1;
dynare_gui_.sensitivity.rmse{num,1} = 'nodisplay';
dynare_gui_.sensitivity.rmse{num,2} = 0;
dynare_gui_.sensitivity.rmse{num,3} = 'check_option';
dynare_gui_.sensitivity.rmse{num,4} = 'Do not display the graphs, but still save them to disk (unless nograph is used).';

num = num + 1;
dynare_gui_.sensitivity.rmse{num,1} = 'graph_format';
dynare_gui_.sensitivity.rmse{num,2} = 'eps';
dynare_gui_.sensitivity.rmse{num,3} = 'eps, pdf, fig, none';
dynare_gui_.sensitivity.rmse{num,4} = 'Specify the file format(s) for graphs saved to disk. Possible values are eps (the default), pdf, fig and none (under Octave, only eps and none are available). If the file format is set equal to none, the graphs are displayed but not saved to the disk.';

num = num + 1;
dynare_gui_.sensitivity.rmse{num,1} = 'conf_sig';
dynare_gui_.sensitivity.rmse{num,2} = '';
dynare_gui_.sensitivity.rmse{num,3} = 'DOUBLE';
dynare_gui_.sensitivity.rmse{num,4} = 'Confidence interval used for classical forecasting after estimation.';

num = num + 1;
dynare_gui_.sensitivity.rmse{num,1} = 'loglinear';
dynare_gui_.sensitivity.rmse{num,2} = 0;
dynare_gui_.sensitivity.rmse{num,3} = 'check_option';
dynare_gui_.sensitivity.rmse{num,4} = 'Computes a log-linear approximation of the model instead of a linear approximation. As always in the context of estimation, the data must correspond to the definition of the variables used in the model (see Pfeifer 2013 for more details on how to correctly specify observation equations linking model variables and the data). If you specify the loglinear option, Dynare will take the logarithm of both your model variables and of your data as it assumes the data to correspond to the original non-logged model variables. The displayed posterior results like impulse responses, smoothed variables, and moments will be for the logged variables, not the original un-logged ones. Default: computes a linear approximation.';

num = num + 1;
dynare_gui_.sensitivity.rmse{num,1} = 'mode_file';
dynare_gui_.sensitivity.rmse{num,2} = '';
dynare_gui_.sensitivity.rmse{num,3} = 'FILENAME';
dynare_gui_.sensitivity.rmse{num,4} = 'Name of the file containing previous value for the mode. When computing the mode, Dynare stores the mode (xparam1) and the hessian (hh, only if cova_compute=1) in a file called MODEL_FILENAME_mode.mat.';

num = num + 1;
dynare_gui_.sensitivity.rmse{num,1} = 'kalman_algo';
dynare_gui_.sensitivity.rmse{num,2} = '0';
dynare_gui_.sensitivity.rmse{num,3} = 'INTEGER';
dynare_gui_.sensitivity.rmse{num,4} = '0: Automatically use the Multivariate Kalman Filter for stationary models and the Multivariate Diffuse Kalman Filter for non-stationary models; 1: Use the Multivariate Kalman Filter 2: Use the Univariate Kalman Filter; 3: Use the Multivariate Diffuse Kalman Filter; 4: Use the Univariate Diffuse Kalman Filter; Default value is 0. In case of missing observations of single or all series, Dynare treats those missing values as unobserved states and uses the Kalman filter to infer their value.';

%% Group 5: Identification
num = 1;
dynare_gui_.sensitivity.identification{num,1} = 'identification';
dynare_gui_.sensitivity.identification{num,2} = 0;
dynare_gui_.sensitivity.identification{num,3} = 'check_option';
dynare_gui_.sensitivity.identification{num,4} = 'If equal to 1, performs identification anlysis (forcing redform=0 and morris=1) If equal to 0, no identification analysis.';

num = num + 1;
dynare_gui_.sensitivity.identification{num,1} = 'load_ident_files';
dynare_gui_.sensitivity.identification{num,2} = '0';
dynare_gui_.sensitivity.identification{num,3} = 'INTEGER';
dynare_gui_.sensitivity.identification{num,4} = 'Loads previously performed identification analysis.';

num = num + 1;
dynare_gui_.sensitivity.identification{num,1} = 'useautocorr';
dynare_gui_.sensitivity.identification{num,2} = 0;
dynare_gui_.sensitivity.identification{num,3} = 'check_option';
dynare_gui_.sensitivity.identification{num,4} = 'Use autocorrelation matrices in place of autocovariance matrices in moments for identification analysis.';

num = num + 1;
dynare_gui_.sensitivity.identification{num,1} = 'ar';
dynare_gui_.sensitivity.identification{num,2} = '1';
dynare_gui_.sensitivity.identification{num,3} = 'INTEGER';
dynare_gui_.sensitivity.identification{num,4} = 'Maximum number of lags for moments in identification analysis.';

num = num + 1;
dynare_gui_.sensitivity.identification{num,1} = 'diffuse_filter';
dynare_gui_.sensitivity.identification{num,2} = 0;
dynare_gui_.sensitivity.identification{num,3} = 'check_option';
dynare_gui_.sensitivity.identification{num,4} = 'Uses the diffuse Kalman filter (as described in Durbin and Koopman (2012) and Koopman and Durbin (2003)) to estimate models with non-stationary observed variables.';

end