function status = set_command_option(name, value)
% function status = set_command_option(name, value)
% auxiliary function which sets the command option
%
% INPUTS
%   name:   command option name
%   value:  command option value
%
% OUTPUTS
%   status: status of performed operation
%
% SPECIAL REQUIREMENTS
%   none

% Copyright (C) 2003-2020 Dynare Team
%
% This file is part of Dynare.
%
% Dynare is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% Dynare is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with Dynare.  If not, see <http://www.gnu.org/licenses/>.

global options_

status = 1;

if strcmp(name, 'graph_format')
    if ~ischar(value)
        status = 0;
        return
    end
    value = strsplit(value, ',')';
end

try
    switch name
        case 'lyapunov'
            options_.lyapunov_fp = 0;
            options_.lyapunov_db = 0;
            options_.lyapunov_srs = 0;

            if value == 3 % fixed_point
                options_.lyapunov_fp = 1;
            elseif value == 4 %doubling
                options_.lyapunov_db = 1;
            elseif value == 5 %square_root_solver
                options_.lyapunov_srs = 1;
            end

        case 'sylvester'
            options_.sylvester_fp = 0;
            
            if value == 2 %fixed_point
                options_.sylvester_fp = 1;
            end
            
        case 'dr'
            options_.dr_cycle_reduction = 0;
            options_.dr_logarithmic_reduction = 0;

            if value == 2
                options_.dr_cycle_reduction = 1;
            elseif value == 3
                options_.dr_logarithmic_reduction = 1;
            end

        case 'first_obs'
            options_.first_obs = str2num(value);

        case 'use_bandpass_filter'
            options_.bandpass.indicator = value;
            
        case 'bandpass_filter_passband'
            options_.bandpass.passband = value;

        case 'lmmcp'
            options_.lmmcp.status = value;
            
        case 'nobs'
            options_.nobs = str2num(value);
            
        case 'consider_only_observed'
            return;
            
        case 'mode_compute'
            if ~isempty(str2num(value))
                options_.mode_compute = str2num(value);
            else
                options_.mode_compute = value;
            end

        case 'filter_step_ahead'
            options_.filter_step_ahead = str2num(value);
            
        case 'mcmc_jumping_covariance'
            if value == 2
                options_.(gui_auxiliary.command_option_mapping(name)) = 'prior_variance';
            elseif value == 3
                options_.(gui_auxiliary.command_option_mapping(name)) = 'identity_matrix';
            elseif value == 4
                options_.(gui_auxiliary.command_option_mapping(name)) = 'FILENAME';
            end
         
        case 'mcmc_jumping_covariance_file'
            if ~isempty(value) && strcmp(options_.MCMC_jumping_covariance, 'FILENAME')
                options_.MCMC_jumping_covariance = strrep(value, '.mat', '');
            end
            
        case 'mh_recover'
            options_.mh_recover = logical(value);
            
        case 'raftery_lewis_diagnostics'
            options_.convergence.rafterylewis.indicator = logical(value);

        case 'raftery_lewis_qrs'
            options_.convergence.rafterylewis.qrs = str2num(value);

        case 'posterior_sampling_method'
            if value == 2
                options_.posterior_sampler_options.posterior_sampling_method = 'tailored_random_block_metropolis_hastings';
            elseif value == 3
                options_.posterior_sampler_options.posterior_sampling_method = 'independent_metropolis_hastings';
            elseif value == 4
                options_.posterior_sampler_options.posterior_sampling_method = 'slice';
            end
            
        case 'posterior_sampler_options'
            options_.posterior_sampler_options.sampling_opt = value;
            
        case 'optim'
            options_.optim_opt = value;
            
        case 'resampling'
            if value == 2
                options_.particle.resampling.status.systematic = false;
                options_.particle.resampling.status.none = true;
            elseif value == 3
                options_.particle.resampling.status.systematic = false;
                options_.particle.resampling.status.generic = true;
            end
            
        case 'resampling_method'
            if value == 2
                options_.particle.resampling.method.kitigawa = false;
                options_.particle.resampling.method.stratified = true;
            elseif value == 3
                options_.particle.resampling.method.kitigawa = false;
                options_.particle.resampling.method.smooth = true;
            end
            
        case 'filter_algorithm'
            if value == 2
                options_.particle.filter_algorithm = 'apf';
            elseif value == 3
                options_.particle.filter_algorithm = 'gf';
            elseif value == 4
                options_.particle.filter_algorithm = 'gmf';
            elseif value == 5
                options_.particle.filter_algorithm = 'cpf';
            elseif value == 6
                options_.particle.filter_algorithm = 'nlkf';
            end
         
        case 'distribution_approximation'
            if value == 2
                options_.particle.distribution_approximation.unscented = false;
                options_.particle.distribution_approximation.cubature = true;
            elseif value == 3
                options_.particle.distribution_approximation.unscented = false;
                options_.particle.distribution_approximation.montecarlo = true;
            end
            
        case 'proposal_approximation'
            if value == 2
                options_.particle.proposal_approximation.unscented = false;
                options_.particle.proposal_approximation.cubature = true;
            elseif value == 3
                options_.particle.proposal_approximation.unscented = false;
                options_.particle.proposal_approximation.montecarlo = true;
            end
            
        case 'cpf_weights'
            if value == 2
                options_.particle.cpf_weights_method.amisanotristani = false;
                options_.particle.cpf_weights_method.murrayjonesparslow = true;
            end

        case 'posterior_nograph'
            options_.no_graph.posterior = logical(value);
   
        case 'irf_shocks'
            if contains(value, ',')
                irf_shocks_ = strsplit(value, ',');
                options_.irf_shocks = irf_shocks_;
            else
                options_.irf_shocks = {value};
            end

        case 'keep_kalman_algo_if_singularity_is_detected'
            options_.kalman.keep_kalman_algo_if_singularity_is_detected = logical(value);

        otherwise
            if isnumeric(value)
                eval(['options_.' gui_auxiliary.command_option_mapping(name) '=' num2str(value) ';']);
            else
                eval_expr = sprintf("options_.%s='%s';", gui_auxiliary.command_option_mapping(name), value);
                eval(eval_expr);
            end
    end
catch
    status = 0;
end
end
