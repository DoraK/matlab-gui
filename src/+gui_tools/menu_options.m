function menu_options(oid, status)
% function menu_options(oid, status)
% auxiliary function which enables and disables menu options
%
% INPUTS
%   oid: operation indicator which represents group of menu options
%   status: status indicator (On or Off)
%
% OUTPUTS
%   none
%
% SPECIAL REQUIREMENTS
%   none

% Copyright (C) 2003-2020 Dynare Team
%
% This file is part of Dynare.
%
% Dynare is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% Dynare is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with Dynare.  If not, see <http://www.gnu.org/licenses/>.

global oo_ project_info

handles = guihandles(getappdata(0,'main_figure'));

switch oid
    case 'project'
        handles.project_save.Enable = status;
        handles.project_save_as.Enable = status;
        handles.project_close.Enable = status;
        handles.model_load.Enable = status;

    case 'model'
        handles.model_logfile.Enable = status;
        handles.model_settings.Enable = status;
        handles.model_save_snapshot.Enable = status;
        handles.model_load_snapshot.Enable = status;

    case 'model_special'
        handles.model_settings.Enable = status;
        handles.model_save_snapshot.Enable = status;
        handles.model_load_snapshot.Enable = status;

    case 'estimation'
        handles.estimation_observed_variables.Enable = status;
        handles.estimation_parameters_shocks.Enable = status;
        handles.estimation_run_calibrated_smoother.Enable = status;
        handles.estimation_run.Enable = status;
        if project_info.estimation_run
            handles.estimation_run_smoother2histval.Enable = status;
        end

    case 'stochastic'
        handles.model_export.Enable = status;
        handles.simulation_stochastic.Enable = status;

    case 'deterministic'
        handles.model_export.Enable = status;
        handles.simulation_deterministic.Enable = status;

    case 'sensitivity'
        handles.model_export.Enable = status;
        handles.sensitivity_analysis.Enable = status;
        handles.irf_calibration.Enable = status;
        handles.moment_calibration.Enable = status;

    case 'output'
        if strcmpi(status, 'off')
            handles.output_shocks_dec.Enable = 'off';
            handles.output_shocks_decomposition.Enable = 'off';
            handles.output_realtime_shocks_decomposition.Enable = 'off';
            handles.output_conditional_forecast.Enable = 'off';
            handles.output_forecast.Enable = 'off';
        else
            if project_info.estimation_run
                handles.output_shocks_dec.Enable = status;
                handles.output_shocks_decomposition.Enable = status;
                handles.output_realtime_shocks_decomposition.Enable = status;
                if isfield(oo_, 'SmoothedVariables')
                    handles.output_forecast.Enable = status;
                    handles.output_conditional_forecast.Enable = status;
                end
            end
            if project_info.stochastic_simulation_run
                handles.output_forecast.Enable = status;
                handles.output_conditional_forecast.Enable = status;
                if project_info.observed_variables_set
                    handles.output_shocks_dec.Enable = status;
                    handles.output_shocks_decomposition.Enable = status;
                    handles.output_realtime_shocks_decomposition.Enable = status;
                end
            end
            if (project_info.realtime_shock_decomposition_run || project_info.shock_decomposition_run) ...
                    && project_info.observed_variables_set
                handles.output_shocks_dec.Enable = status;
                handles.output_plot_shocks_decomposition.Enable = status;
            end
        end

    case 'epilogue'
        handles.post_processing_epilogue.Enable = status;
        
    case 'posterior'
        handles.post_processing_posterior.Enable = status;

    otherwise
        error(['menu option: ' oid ' not handled']);
end
end
