function comm_str = command_string(comm_name, comm)
% function comm_str = command_string(comm_name, comm)
% auxiliary function which creates string representation for specified
% command and its options
%
% INPUTS
%   comm_name: command name
%   comm: structure which holds defined command options
%
% OUTPUTS
%   comm_str: string representation for specified command and its options
%
% SPECIAL REQUIREMENTS
%   none

% Copyright (C) 2003-2020 Dynare Team
%
% This file is part of Dynare.
%
% Dynare is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% Dynare is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with Dynare.  If not, see <http://www.gnu.org/licenses/>.

global project_info

if strcmp(comm_name, 'dynare')
    comm_str = [comm_name ' ' project_info.mod_file ' gui -DGUI '];
    if project_info.project_model_stochastic
        comm_str = [comm_str 'stochastic '];
    end
    opt_sep = ' ';
else
    comm_str = [comm_name '('];
    opt_sep = ', ';
end

if isempty(comm)
    if ~strcmp(comm_name, 'dynare')
        comm_str = [comm_str ')'];
    end
    return
end

first_concat = true;
use_bandpass = false;
names = fieldnames(comm);
for ii = 1:size(names, 1)
    value = comm.(names{ii});
    option_type = gui_tools.get_option_type(comm_name, names{ii});

    if strcmp(names{ii}, 'use_bandpass_filter') && value == 1
        use_bandpass = true;
    end
    if ~use_bandpass && strcmp(names{ii}, 'bandpass_filter_passband')
        continue
    end
    if isempty(value)
        option = names{ii};
    elseif iscell(option_type)
        option = [names{ii} '=' option_type{value}];
    elseif ~isempty(option_type) && strcmp(option_type,'check_option')
        option = names{ii};
    elseif isa(value,'double')
        option = sprintf('%s=%g',names{ii},value);
    elseif ~isempty(option_type) && strcmp(option_type,'INPUT')
        if strcmp(names{ii}, 'IPATH')
            option = sprintf('-I<<%s>>',value);
        else
            option = sprintf('%s',value);
        end
    else
        if ~isempty(strfind(value,','))
            option = sprintf('%s=(%s)',names{ii},value);
        else
            option = sprintf('%s=%s',names{ii},value);
        end
    end

    if ~isempty(option)
        if first_concat
            comm_str = [comm_str option];
            first_concat = false;
        else
            comm_str = [comm_str opt_sep option];
        end
    end
end

if ~strcmp(comm_name, 'dynare')
    comm_str = [comm_str ')'];
end
end
