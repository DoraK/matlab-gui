function gui_cond_forecast(tabId)
% function gui_cond_forecast(tabId)
% interface for the DYNARE imcforecast command
%
% INPUTS
%   tabId:  GUI tab element which displays imcforecast command interface
%
% OUTPUTS
%   none
%
% SPECIAL REQUIREMENTS
%   none

% Copyright (C) 2003-2019 Dynare Team
%
% This file is part of Dynare.
%
% Dynare is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% Dynare is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with Dynare.  If not, see <http://www.gnu.org/licenses/>.

global project_info model_settings dynare_gui_
global oo_ options_ M_

bg_color = char(getappdata(0, 'bg_color'));
special_color = char(getappdata(0, 'special_color'));

handles = [];
gui_size = gui_tools.get_gui_elements_size(tabId);

% Constraint Paths
handles.constraints = uitabgroup(tabId, 'Position', [0.01 0.18 0.48 0.73]);
handles.constraint_tabs = containers.Map( ...
    model_settings.conditional_forecast_options.endo, ...
    repmat({struct}, length(model_settings.conditional_forecast_options.endo), 1));
for i = 1:length(model_settings.conditional_forecast_options.endo)
    endo = model_settings.conditional_forecast_options.endo{i};
    tab = uitab(handles.constraints, 'Title', endo);
    switch model_settings.conditional_forecast_options.baseline_source
        case 'File'
            disp('not yet supported')
            return
        case 'Forecast'
            baseline_data = cell(length(oo_.forecast.Mean.(endo)), 3);
            baseline_data(:, 1) = num2cell(oo_.forecast.Mean.(endo));
        case 'Mean Forecast'
            baseline_data = cell(length(oo_.MeanForecast.(endo)), 3);
            baseline_data(:, 1) = num2cell(oo_.MeanForecast.(endo));
    end
    handles.constraint_tabs(endo) = uitable( ...
        tab, ...
        'Units', 'normalized', ...
        'Position', [0 0 1 1], ...
        'ColumnName', {'Baseline', 'New Value', 'Clear'}, ...
        'ColumnFormat', {'numeric', 'numeric', 'logical'}, ...
        'ColumnEditable', [false true true], ...
        'Data', baseline_data, ...
        'RowName', 'numbered');
end

handles.uipanelVars = uipanel( ...
    'Parent', tabId, ...
    'Tag', 'uipanelVars', 'BackgroundColor', special_color,...
    'Units', 'normalized', 'Position', [0.51 0.18 0.48 0.73], ...
    'Title', '', 'BorderType', 'none');

handles = gui_tabs.create_endo_vars(handles, 'conditional_forecast');

% Set Command
handles.uipanelComm = uipanel( ...
    'Parent', tabId, ...
    'Tag', 'uipanelCommOptions', ...
    'UserData', zeros(1,0),'BackgroundColor', bg_color, ...
    'Units', 'normalized', 'Position', [0.01 0.09 0.98 0.09], ...
    'Title', 'Current command options:');
comm_str = set_comm_str();
handles.conditional_forecast = uicontrol( ...
    'Parent', handles.uipanelComm, ...
    'Tag', 'stoch_simul', ...
    'Style', 'text', 'BackgroundColor', bg_color,...
    'Units', 'normalized', 'Position', [0.01 0.01 0.98 0.98], ...
    'FontAngle', 'italic', ...
    'String', comm_str, ...
    'TooltipString', comm_str, ...
    'HorizontalAlignment', 'left');

% Buttons
handles.pushbuttonCondForecast = uicontrol( ...
    'Parent', tabId, ...
    'Tag', 'pushbuttonCondForecast', ...
    'Style', 'pushbutton', ...
    'Units','normalized','Position',[gui_size.space gui_size.bottom gui_size.button_width gui_size.button_height],...
    'String', 'Run conditional forecast', ...
    'Callback', @pushbuttonCondForecast_Callback);

handles.pushbuttonReset = uicontrol( ...
    'Parent', tabId, ...
    'Tag', 'pushbuttonReset', ...
    'Style', 'pushbutton', ...
    'Units','normalized','Position',[gui_size.space*2+gui_size.button_width gui_size.bottom gui_size.button_width gui_size.button_height],...
    'String', 'Reset', ...
    'Callback', @pushbuttonReset_Callback);

handles.pushbuttonClose = uicontrol( ...
    'Parent', tabId, ...
    'Tag', 'pushbuttonReset', ...
    'Style', 'pushbutton', ...
    'Units','normalized','Position',[gui_size.space*3+gui_size.button_width*2 gui_size.bottom gui_size.button_width gui_size.button_height],...
    'String', 'Close this tab', ...
    'Callback',{@close_tab,tabId});

handles.pushbuttonCloseAll = uicontrol( ...
    'Parent', tabId, ...
    'Tag', 'pushbuttonSimulation', ...
    'Style', 'pushbutton', ...
    'Units','normalized','Position',[gui_size.space*4+gui_size.button_width*3 gui_size.bottom gui_size.button_width gui_size.button_height],...
    'String', 'Close all output figures', ...
    'Enable', 'on',...
    'Callback', @pushbuttonCloseAll_Callback);

handles.pushbuttonCommandDefinition = uicontrol( ...
    'Parent', tabId, ...
    'Tag', 'pushbuttonCommandDefinition', ...
    'Style', 'pushbutton', ...
    'Units','normalized','Position',[1-gui_size.space-gui_size.button_width gui_size.bottom gui_size.button_width gui_size.button_height],...
    'String', 'Define command options ...', ...
    'Callback', @pushbuttonCommandDefinition_Callback);

    function pushbuttonCondForecast_Callback(hObject, ~)
        % Check options
        new_comm = getappdata(0, 'conditional_forecast');
        if ~isfield(new_comm, 'parameter_set')
            gui_tools.show_warning('Please select parameter set');
            uicontrol(hObject);
            return
        end
        
        var_list_ = handles.endoTable.Data([handles.endoTable.Data{:, 1}], 2);
        if isempty(var_list_)
            gui_tools.show_warning('Please chose at least one endogenous variable to plot');
            uicontrol(hObject);
            return
        end
        model_settings.varlist_.conditional_forecast = var_list_;

        % Set Constrained Paths
        npaths = length(model_settings.conditional_forecast_options.endo);
        model_settings.constrained_vars_ = zeros(npaths, 1);
        for idx = 1:length(model_settings.conditional_forecast_options.endo)
            model_settings.constrained_vars_(idx) = find(strcmp(model_settings.conditional_forecast_options.endo{idx}, M_.endo_names));
        end
        periods = length(handles.constraint_tabs(model_settings.conditional_forecast_options.endo{idx}).Data(:,1));
        model_settings.constrained_paths_ = NaN(npaths, periods);
        for idx = 1:npaths
            model_settings.constrained_paths_(idx, :) = ...
                cell2mat(handles.constraint_tabs(model_settings.conditional_forecast_options.endo{idx}).Data(:, 1));
            overwriteidxs = ~cellfun(@isempty, handles.constraint_tabs(model_settings.conditional_forecast_options.endo{idx}).Data(:, 2));
            model_settings.constrained_paths_(idx, overwriteidxs) = cell2mat(handles.constraint_tabs(model_settings.conditional_forecast_options.endo{idx}).Data(:, 2));
            overwriteidxs = ~cellfun(@isempty, handles.constraint_tabs(model_settings.conditional_forecast_options.endo{idx}).Data(:, 3));
            model_settings.constrained_paths_(idx, overwriteidxs) = NaN(1, sum(overwriteidxs));
        end

        old_options = options_;
        options_.datafile = project_info.data_file;
        options_.nodisplay = 0;

        gui_tools.project_log_entry('Running conditional forecast','...');
        [jObj, guiObj] = gui_tools.create_animated_screen('Running conditional forecast, please wait...', tabId);

        % Set Options from Define
        options_cond_fcst_ = struct();
        names = fieldnames(model_settings.conditional_forecast);
        for idx = 1:length(names)
            if strcmp(names{idx}, 'parameter_set')
                popupmenu = dynare_gui_.conditional_forecast.setup{strcmp('parameter_set', dynare_gui_.conditional_forecast.setup(:, 1)), 3};
                options_cond_fcst_.(names{idx}) = popupmenu{model_settings.conditional_forecast.(names{idx})};
            else
                options_cond_fcst_.(names{idx}) = model_settings.conditional_forecast.(names{idx});
            end
        end
        
        % Set controlled varexo
        options_cond_fcst_.controlled_varexo = model_settings.conditional_forecast_options.controlled_varexo;

        try
            imcforecast(model_settings.constrained_paths_, model_settings.constrained_vars_, options_cond_fcst_);
            if isfield(options_cond_fcst_, 'plot_periods')
                plot_periods = options_cond_fcst_.plot_periods;
            else
                plot_periods = periods;
            end

            plot_icforecast(model_settings.varlist_.conditional_forecast, plot_periods, options_);

            jObj.stop;
            jObj.setBusyText('All done!');
            project_info.modified = true;
            project_info.conditional_forecast_run = true;
        catch ME
            jObj.stop;
            jObj.setBusyText('Done with errors');
            gui_tools.show_error('Error in execution of conditional forecast command', ME, 'extended');
            uicontrol(hObject);
        end
        delete(guiObj);
        options_ = old_options;
    end

    function pushbuttonReset_Callback(~, ~)
        handles.endoTable.Data(:, 1) = {false};
        for idx = 1:length(model_settings.conditional_forecast_options.endo)
            endo = model_settings.conditional_forecast_options.endo{idx};
            d = handles.constraint_tabs(endo);
            d.Data(:, 2:3) = repmat({[]}, size(d.Data, 1), 2);
            handles.constraint_tabs(endo) = d;
        end
        comm_str = set_comm_str();
        handles.conditional_forecast.String = comm_str;
        handles.conditional_forecast.TooltipString = comm_str;
        model_settings.conditional_forecast = struct();
    end

    function close_tab(~, ~, hTab)
        gui_tabs.delete_tab(hTab);
    end

    function pushbuttonCloseAll_Callback(~, ~)
        gui_tools.close_all_figures();
    end

    function pushbuttonCommandDefinition_Callback(~, ~)
        h = gui_define_comm_options(dynare_gui_.conditional_forecast, 'conditional_forecast');
        uiwait(h);
        try
            new_comm = getappdata(0, 'conditional_forecast');
            if ~isempty(new_comm)
                model_settings.conditional_forecast = new_comm;
            end
            comm_str = set_controlled_varexo(gui_tools.command_string('conditional_forecast', new_comm));
            handles.conditional_forecast.String = comm_str;
            handles.conditional_forecast.TooltipString = comm_str;
            gui_tools.project_log_entry('Defined command conditional_forecast', comm_str);
        catch ME
            gui_tools.show_error('Error in defining conditional_forecast command', ME, 'basic');
        end
    end

    function comm_str = set_comm_str()
        comm_str = set_controlled_varexo('conditional_forecast(');
    end

    function comm_str = set_controlled_varexo(comm_str)
        if strcmp(comm_str(:,end), ')')
            comm_str = comm_str(1:end-1);
        end
        if ~strcmp(comm_str(:,end), '(')
            comm_str = [comm_str ','];
        end
        comm_str = [comm_str ...
            'controlled_varexo=(' ...
            strjoin(model_settings.conditional_forecast_options.controlled_varexo, ',') ...
            '))'];
    end
end
