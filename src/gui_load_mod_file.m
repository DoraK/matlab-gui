function gui_load_mod_file(hObject)
% function gui_load_mod_file(hObject)
% interface for loading and executing .mod/.dyn file with dynare command
%
% INPUTS
%   hObject:    handle of main application window
%
% OUTPUTS
%   none
%
% SPECIAL REQUIREMENTS
%   none

% Copyright (C) 2003-2020 Dynare Team
%
% This file is part of Dynare.
%
% Dynare is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% Dynare is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with Dynare.  If not, see <http://www.gnu.org/licenses/>.

global project_info model_settings dynare_gui_
global M_ options_ oo_

bg_color = char(getappdata(0,'bg_color'));

handles = [];
new_project = false;

dynare_command = struct();
if ~isfield(project_info, 'mod_file') || isempty(project_info.mod_file)
    tab_title = '.mod file';
    comm_str = '';
    project_info.dynare_command = dynare_command;
else
    tab_title = project_info.mod_file;
    if ~isfield(model_settings,'dynare')
        project_info.dynare_command = dynare_command;
    end
    comm_str = gui_tools.command_string('dynare', project_info.dynare_command);
end

% Set menus properly
gui_set_menus(true);

[tabId, ~] = gui_tabs.add_tab(hObject, tab_title);
gui_size = gui_tools.get_gui_elements_size(tabId);

uicontrol(tabId, ...
    'Style', 'pushbutton', ...
    'String', 'Edit .mod/.dyn file', ...
    'Units', 'normalized', ...
    'Position', [gui_size.space 1-gui_size.bottom-gui_size.button_height gui_size.button_width gui_size.button_height], ...
    'Callback', @pushbuttonEditModFile);

handles.uipanelComm = uipanel( ...
    'Parent', tabId, ...
    'Tag', 'uipanelCommOptions', ...
    'UserData', zeros(1,0), 'BackgroundColor', bg_color, ...
    'Units', 'normalized', 'Position', [0.01 0.09 0.98 0.09], ...
    'Title', 'Current command options:');

handles.dynare = uicontrol( ...
    'Parent', handles.uipanelComm, ...
    'Style', 'text', 'BackgroundColor', bg_color,...
    'Units', 'normalized', 'Position', [0.01 0.01 0.98 0.98], ...
    'FontAngle', 'italic', ...
    'String', comm_str, ...
    'TooltipString', comm_str, ...
    'HorizontalAlignment', 'left');

handles.runModFile = uicontrol(tabId, 'Style','pushbutton','String','Run .mod/.dyn file','Units','normalized','Position',[gui_size.space gui_size.bottom gui_size.button_width gui_size.button_height], 'Callback',{@run_file,tabId} );
uicontrol(tabId, 'Style','pushbutton','String','Specify .mod/.dyn file','Units','normalized','Position',[gui_size.space*2+gui_size.button_width gui_size.bottom gui_size.button_width gui_size.button_height], 'Callback',{@change_file,tabId} );
uicontrol(tabId, 'Style','pushbutton','String','Close this tab','Units','normalized','Position',[gui_size.space*3+gui_size.button_width*2 gui_size.bottom gui_size.button_width gui_size.button_height], 'Callback',{@close_tab,tabId} );
uicontrol(tabId, 'Style','pushbutton','String','Define command options ...','Units','normalized','Position',[1-gui_size.space-gui_size.button_width gui_size.bottom gui_size.button_width gui_size.button_height], 'Callback',@pushbuttonCommandDefinition_Callback);

% first check if .mod file already specified
if ~isfield(project_info, 'mod_file') || isempty(project_info.mod_file)
    handles.runModFile.Enable = 'Off';
    new_project = true;

    if isempty([dir('*.mod'); dir('*.dyn')])
        gui_tools.show_error('You must have chosen a project directory that already contains a .mod/.dyn file');
        return
    elseif length([dir('*.mod'); dir('*.dyn')]) == 1
        fileName = [dir('*.mod'); dir('*.dyn')];
        project_info.mod_file = fileName.name;
        [~, project_info.model_name, ~] = fileparts(project_info.mod_file);
    else
        status = specify_file(true);
        if status == 0
            return
        end
    end

    handles.runModFile.Enable = 'On';
    gui_tabs.rename_tab(tabId, project_info.mod_file);
    comm_str = gui_tools.command_string('dynare', project_info.dynare_command);

    set(handles.dynare, 'String', comm_str);
    set(handles.dynare, 'TooltipString', comm_str);
end

if project_info.mod_file_already_run
    [~, ~, ext] = fileparts(project_info.mod_file);
    if isempty(ext)
        % Enter here if the mod file was run before opening the GUI
        % i.e. if M_, options_, oo_ exist in the workspace
        % as determined in gui_project.m
        if exist([project_info.mod_file '.mod'], 'file') == 2
            project_info.mod_file = [project_info.mod_file '.mod'];
        elseif exist([project_info.mod_file '.dyn'], 'file') == 2
            project_info.mod_file = [project_info.mod_file '.dyn'];
        else
            gui_tools.show_error('Can''t find specified file');
        end
    end
end

    function status = specify_file(new_project)
        fileName = uigetfile({'*.mod'; '*.dyn'}, 'Select .mod or .dyn file');
        if fileName == 0
            status = 0;
        else
            status = 1;
            project_info.mod_file = fileName;
            [~, project_info.model_name, ~] = fileparts(project_info.mod_file);
            gui_tabs.rename_tab(tabId, project_info.model_name);
        end
    end

    function change_file(~, ~, hTab)
        project_info.mod_file = [];
        status = specify_file(new_project);
        if status ~= 0
            gui_tabs.rename_tab(hTab, project_info.mod_file);
            comm_str = gui_tools.command_string('dynare', project_info.dynare_command);
            handles.dynare.String = comm_str;
            handles.dynare.TooltipString = comm_str;
            project_info.mod_file_already_run = false;
            %disable menu options
            gui_set_menus(false);
            % close other tabs
            gui_tabs.close_all_except_this(tabId);
        end
    end

    function close_tab(~, ~, hTab)
        gui_tabs.delete_tab(hTab);
    end

    function run_file(~, ~, ~)
        if project_info.mod_file_already_run
            answer = questdlg({'Do you want to run .mod/.dyn file with Dynare?'; '';...
                'It will change all Dynare structures (oo_, M_, options_, etc) and discard results of your project.'},...
                'Dynare_GUI','Yes','No','No');
            if strcmp(answer,'No')
                return
            end
            clear oo_ M_ options_ estim_params_ bayestopt_ estimation_info ex0_ ys0_ dataset_ dataset_info
        end

        %save globals
        glob_project_info = project_info;
        glob_model_settings = model_settings;
        glob_dynare_gui_ = dynare_gui_;
        
        [jObj, guiObj] = gui_tools.create_animated_screen(['Running ' project_info.mod_file ', please wait...'], tabId);

        try
            disp(comm_str)
            eval([comm_str ' json=compute'])
            try
                steady;
                oo_.dr.eigval = check(M_, options_, oo_);
            catch
                gui_tools.show_warning('Couldn''t find the steady state, so you need to fill in Model Settings->Steady State Guess before doing anything else');
            end
            jObj.stop;
            jObj.setBusyText('Done');

            %restore GUI globals
            evalin('base', 'global dynare_gui_ project_info model_settings');
            dynare_gui_ = glob_dynare_gui_;
            project_info = glob_project_info;
            project_info.modified = true;
            project_info.mod_file_already_run  = true;
            
            % recreate model_settings
            gui_create_model_settings();

            %enable menu options
            gui_set_menus(true);
            gui_tools.project_log_entry('Running .mod/.dyn file', ['mod_file=' project_info.mod_file]);
        catch ME
            %restore globals
            evalin('base','global dynare_gui_ project_info model_settings');
            dynare_gui_ = glob_dynare_gui_;
            project_info = glob_project_info;
            model_settings = glob_model_settings;

            jObj.stop;
            jObj.setBusyText('Done with errors');
            gui_tools.show_error('Error in execution of dynare command', ME, 'extended');
        end
        delete(guiObj);
    end

    function pushbuttonCommandDefinition_Callback(~, ~)
        h = gui_define_comm_options(dynare_gui_.dynare, 'dynare');
        uiwait(h);
        try
            new_comm = getappdata(0, 'dynare');
            if ~isempty(new_comm)
                project_info.dynare_command = new_comm;
                comm_str = gui_tools.command_string('dynare', new_comm);
                set(handles.dynare, 'String', comm_str);
                set(handles.dynare, 'TooltipString', comm_str);
                gui_tools.project_log_entry('Defined dynare command', comm_str);
            end
        catch ME
            gui_tools.show_error('Error defining dynare command', ME, 'basic');
        end
    end

    function pushbuttonEditModFile(~, ~)
        edit(project_info.mod_file);
    end
end
