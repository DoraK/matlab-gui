function gui_sensitivity_analysis(tabId)
% function gui_sensitivity_analysis(tabId)
% interface for the DYNARE dynare_sensitivity command
%
% INPUTS
%   tabId:      GUI tab element which displays sensitivity command interface
%
% OUTPUTS
%   none
%
% SPECIAL REQUIREMENTS
%   none

% Copyright (C) 2019-2020 Dynare Team
%
% This file is part of Dynare.
%
% Dynare is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% Dynare is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with Dynare.  If not, see <http://www.gnu.org/licenses/>.

global project_info model_settings options_ dynare_gui_ oo_

bg_color = char(getappdata(0, 'bg_color'));

handles = [];
gui_size = gui_tools.get_gui_elements_size(tabId);

do_not_check_all_results = 0;

if ~isfield(model_settings, 'sensitivity')
    model_settings.sensitivity = struct();
end
comm_str = gui_tools.command_string('dynare_sensitivity', model_settings.sensitivity);

handles.uipanelComm = uipanel( ...
    'Parent', tabId, ...
    'Tag', 'uipanelCommOptions', ...
    'UserData', zeros(1,0), 'BackgroundColor', bg_color, ...
    'Units', 'normalized', 'Position', [0.01 0.09 0.98 0.09], ...
    'Title', 'Current command options:');

handles.sensitivity = uicontrol( ...
    'Parent', handles.uipanelComm, ...
    'Tag', 'sensitivity', ...
    'Style', 'text', 'BackgroundColor', bg_color,...
    'Units', 'normalized', 'Position', [0.01 0.01 0.98 0.98], ...
    'FontAngle', 'italic', ...
    'String', comm_str, ...
    'TooltipString', comm_str, ...
    'HorizontalAlignment', 'left');

handles.pushbuttonSensitivity = uicontrol( ...
    'Parent', tabId, ...
    'Tag', 'pushbuttonSensitivity', ...
    'Style', 'pushbutton', ...
    'Units','normalized','Position',[gui_size.space gui_size.bottom gui_size.button_width_small gui_size.button_height],...
    'String', 'Run sensitivity', ...
    'Callback', @pushbuttonSensitivity_Callback);

    function pushbuttonSensitivity_Callback(~, ~)
        old_oo_ = oo_;
        old_options_ = options_;
        try
            if ~isempty(model_settings.sensitivity)
                names = fieldnames(model_settings.sensitivity);
                options_gsa = model_settings.sensitivity;
                options_ = old_options_;
                for ii = 1:size(names, 1)
                    switch names{ii}
                        case 'ilptau'
                            if options_gsa.(names{ii}) == 2
                                options_gsa.ilptau = 0;
                            end
                        case 'pprior'
                            if options_gsa.(names{ii}) == 2
                                options_gsa.pprior = 0;
                            end
                        case 'prior_range'
                            if options_gsa.(names{ii}) == 2
                                options_gsa.prior_range = 0;
                            end
                        case 'stab'
                            if options_gsa.(names{ii}) == 2
                                options_gsa.stab = 0;
                            end
                        case 'namendo'
                            if contains(options_gsa.(names{ii}), ',')
                                namendo_ = strsplit(options_gsa.(names{ii}), ',');
                                options_gsa.namendo = namendo_;
                            else
                                options_gsa.namendo = {options_gsa.(names{ii})};
                            end
                        case 'namlagendo'
                            if contains(options_gsa.(names{ii}), ',')
                                namlagendo_ = strsplit(options_gsa.(names{ii}), ',');
                                options_gsa.namlagendo = namlagendo_;
                            else
                                options_gsa.namlagendo = {options_gsa.(names{ii})};
                            end
                        case 'namexo'
                            if contains(options_gsa.(names{ii}), ',')
                                namexo_ = strsplit(options_gsa.(names{ii}), ',');
                                options_gsa.namexo = namexo_;
                            else
                                options_gsa.namexo = {options_gsa.(names{ii})};
                            end
                        case 'threshold_redform'
                            options_gsa.threshold_redform = str2num(options_gsa.(names{ii}));
                    end
                end
            end
            gui_tools.project_log_entry('Running stochastic sensitivity','...');
            [jObj, guiObj] = gui_tools.create_animated_screen('Running stochastic sensitivity, please wait...', tabId);
            handles.pushbuttonResults.Enable = 'off';
            dynare_sensitivity(options_gsa);
            jObj.stop;
            jObj.setBusyText('Done');
            handles.pushbuttonResults.Enable = 'on';
            project_info.modified = true;
            project_info.sensitivity_analysis_run = true;
            gui_set_menus(true);
        catch ME
            jObj.stop;
            jObj.setBusyText('Done with errors');
            gui_tools.show_error('Error in execution of sensitivity command', ME, 'extended');
            oo_ = old_oo_;
            options_ = old_options_;
        end
        delete(guiObj);
    end

handles.pushbuttonClose = uicontrol( ...
    'Parent', tabId, ...
    'Tag', 'pushbuttonReset', ...
    'Style', 'pushbutton', ...
    'Units','normalized','Position',[gui_size.space*2+gui_size.button_width_small gui_size.bottom gui_size.button_width_small gui_size.button_height],...
    'String', 'Close this tab', ...
    'Callback',{@close_tab,tabId});

    function close_tab(~, ~, hTab)
        gui_tabs.delete_tab(hTab);
    end

handles.pushbuttonCommandDefinition = uicontrol( ...
    'Parent', tabId, ...
    'Tag', 'pushbuttonCommandDefinition', ...
    'Style', 'pushbutton', ...
    'Units','normalized','Position',[1-gui_size.space-gui_size.button_width_small gui_size.bottom gui_size.button_width_small gui_size.button_height],...
    'String', 'Define command options ...', ...
    'Callback', @pushbuttonCommandDefinition_Callback);

    function pushbuttonCommandDefinition_Callback(~, ~)
        h = gui_define_comm_options(dynare_gui_.sensitivity, 'sensitivity');
        uiwait(h);
        try
            new_comm = getappdata(0, 'sensitivity');
            model_settings.sensitivity = new_comm;
            comm_str = gui_tools.command_string('sensitivity', new_comm);

            handles.sensitivity.String = comm_str;
            gui_tools.project_log_entry('Defined sensitivity command',comm_str);
        catch ME
            gui_tools.show_error('Error defining sensitivity command', ME, 'basic');
        end
    end
end