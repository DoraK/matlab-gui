function gui_set_menus(enable)
% function gui_set_menus()
% takes care of enabling/disabling menus
%
% INPUTS
%   enable    [bool]    if true enable menues otherwise disable
%
% OUTPUTS
%   none
%
% SPECIAL REQUIREMENTS
%   none

% Copyright (C) 2019-2020 Dynare Team
%
% This file is part of Dynare.
%
% Dynare is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% Dynare is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with Dynare.  If not, see <http://www.gnu.org/licenses/>.

global project_info model_settings

gui_tools.menu_options('project', 'On');

gui_tools.menu_options('model', 'Off');
gui_tools.menu_options('model_special','Off');
gui_tools.menu_options('output','Off');
gui_tools.menu_options('estimation','Off');
gui_tools.menu_options('stochastic','Off');
gui_tools.menu_options('deterministic','Off');
gui_tools.menu_options('sensitivity', 'Off');
gui_tools.menu_options('epilogue', 'Off');
gui_tools.menu_options('posterior', 'Off');

if isempty(model_settings) || isempty(fieldnames(model_settings))
    return
end

if enable
    gui_tools.menu_options('model', 'On');
    if isempty(project_info)
        return
    end
    gui_tools.menu_options('epilogue', 'On');
    gui_tools.menu_options('posterior', 'On');
    if project_info.project_model_stochastic
        gui_tools.menu_options('estimation', 'On');
        gui_tools.menu_options('stochastic', 'On');
        gui_tools.menu_options('deterministic', 'Off');
        gui_tools.menu_options('sensitivity', 'On');
    else
        gui_tools.menu_options('estimation', 'Off');
        gui_tools.menu_options('stochastic', 'Off');
        gui_tools.menu_options('deterministic', 'On');
        gui_tools.menu_options('sensitivity', 'Off');
    end
    if project_info.mod_file_already_run
        gui_tools.menu_options('model_special', 'On');
        gui_tools.menu_options('output', 'On');
    end
end
end
